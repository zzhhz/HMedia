package com.zzh.htts.voice;

import android.content.Intent;
import android.media.MediaRecorder;
import android.os.Bundle;

import com.zzh.lib.recorder.ui.AudioRecordFragment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

/**
 * Created by ZZH on 2022/3/22.
 *
 * @Date: 2022/3/22
 * @Email: zzh_hz@126.com
 * @QQ: 1299234582
 * @Author: zzh
 * @Description: 测试 音频录制
 */
public class DemoActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        if (!intent.hasExtra("type")) {
            setContentView(R.layout.act_demo);
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frag_container);
            Bundle args = new Bundle();

            args.putInt(AudioRecordFragment.AUDIO_FORMAT, MediaRecorder.AudioEncoder.HE_AAC);

            fragment.setArguments(args);
        } else {
            setContentView(R.layout.act_21_camera);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
