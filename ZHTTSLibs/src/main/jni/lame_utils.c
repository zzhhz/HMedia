#include "lame_utils.h"
#include "lame/lame.h"
#include <jni.h>

static lame_global_flags *lame = NULL;

JNIEXPORT void JNICALL
Java_com_zzh_lib_recorder_utils_LameUtils_init(JNIEnv *env, jclass clazz, jint inSamplerate,
                                               jint inChannel, jint outSamplerate,
                                               jint outBitrate, jint quality) {

    if (lame != NULL) {
        lame_close(lame);
        lame = NULL;
    }
    lame = lame_init();
    lame_set_in_samplerate(lame, inSamplerate);
    lame_set_num_channels(lame, inChannel);//输入流的声道
    lame_set_out_samplerate(lame, outSamplerate);
    lame_set_brate(lame, outBitrate);
    lame_set_quality(lame, quality);
    lame_init_params(lame);

}

JNIEXPORT jint JNICALL
Java_com_zzh_lib_recorder_utils_LameUtils_encode(JNIEnv *env, jclass clazz, jshortArray buffer_left,
                                                 jshortArray buffer_right, jint samples,
                                                 jbyteArray mp3buf) {
    jshort *j_buffer_l = (*env)->GetShortArrayElements(env, buffer_left, NULL);

    jshort *j_buffer_r = (*env)->GetShortArrayElements(env, buffer_right, NULL);

    const jsize mp3buf_size = (*env)->GetArrayLength(env, mp3buf);
    jbyte *j_mp3buf = (*env)->GetByteArrayElements(env, mp3buf, NULL);

    int result = lame_encode_buffer(lame, j_buffer_l, j_buffer_r,
                                    samples, j_mp3buf, mp3buf_size);

    (*env)->ReleaseShortArrayElements(env, buffer_left, j_buffer_l, 0);
    (*env)->ReleaseShortArrayElements(env, buffer_right, j_buffer_r, 0);
    (*env)->ReleaseByteArrayElements(env, mp3buf, j_mp3buf, 0);

    return result;
}

JNIEXPORT jint JNICALL
Java_com_zzh_lib_recorder_utils_LameUtils_flush(JNIEnv *env, jclass clazz, jbyteArray mp3buf) {
    const jsize mp3buf_size = (*env)->GetArrayLength(env, mp3buf);
    jbyte *j_mp3buf = (*env)->GetByteArrayElements(env, mp3buf, NULL);

    int result = lame_encode_flush(lame, j_mp3buf, mp3buf_size);

    (*env)->ReleaseByteArrayElements(env, mp3buf, j_mp3buf, 0);

    return result;
}

JNIEXPORT void JNICALL
Java_com_zzh_lib_recorder_utils_LameUtils_close(JNIEnv *env, jclass clazz) {
    lame_close(lame);
    lame = NULL;
}