#include <jni.h>
#include <string>
#include "lame/version.h"
#include "lame/lame.h"

extern "C" JNIEXPORT jstring

JNICALL
Java_com_zzh_lib_NativeLib_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    //get_lame_version()
    return env->NewStringUTF(get_lame_version());
}