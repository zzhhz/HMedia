package com.zzh.lib.recorder.def;


import com.zzh.lib.recorder.HMediaRecorder;

import java.io.File;

/**
 * Created by ZZH on 2022/11/18.
 *
 * @Date: 2022/11/18
 * @Email: zzh_hz@126.com
 * @QQ: 1299234582
 * @Author: zzh
 * @Description: 接口
 */
public interface IRecorder {

    /**
     * 开始录音
     */
    void start() throws Exception;

    /**
     * 开始录音
     */
    void start(File file);

    /**
     * 暂停录音
     */
    void pause();

    /**
     * 恢复录音
     */
    void resume();

    /**
     * 停止录音
     */
    void stop();

    /**
     * 释放录音资源
     */
    void release();

    /**
     * 重置录音
     */
    void reset();

    /**
     * 是否正在录音
     *
     * @return true 正在录音
     */
    boolean isRecorder();

    /**
     * 录音文件
     *
     * @return 录音文件
     */
    File getRecordFile();

    IRecorder setRecordFile(File file);

    HMediaRecorder.State getState();
}
