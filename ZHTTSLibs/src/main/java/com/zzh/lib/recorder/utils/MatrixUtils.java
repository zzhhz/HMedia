package com.zzh.lib.recorder.utils;

import android.content.Context;
import android.content.res.Resources;
import android.opengl.Matrix;
import android.util.TypedValue;

import java.io.File;

public class MatrixUtils {

    public static final int RECORD_MIN_TIME = 5 * 1000;

    public static final int EMPTY = 0;

    public static final int DELAY_DELTA = 1;
    public static final int OVER_CLICK = 11;//视频定时结束

    public static int dp2px(final Context ctx, float dip) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip, ctx.getResources().getDisplayMetrics());
    }

    public static int dp2px(int dip) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip, Resources.getSystem().getDisplayMetrics());
    }
}
