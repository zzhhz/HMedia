package com.zzh.lib.recorder.ui;


import android.os.Bundle;

import com.zzh.lib.htts.R;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by zzh on 2023/7/7.
 *
 * @date: 2023/7/7 16:25
 * @email: zzh_hz@126.com
 * @author: zzh
 * @description: com.zzh.lib.recorder.ui 视频录制
 */
public class RecorderActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.h_lib_act_recorder);

        setListener();
    }

    protected void setListener() {
        findViewById(R.id.aiv_close).setOnClickListener(v -> finish());
    }
}
