package com.zzh.lib.recorder.def;

/**
 * Created by ZZH on 2023/1/28.
 *
 * @Date: 2023/1/28
 * @Email: zzh_hz@126.com
 * @QQ: 1299234582
 * @Author: zzh
 * @Description: 异常回调 HMediaRecorder
 */
public interface OnExceptionCallback {
    /**
     * 异常回调
     *
     * @param e
     */
    void onException(Exception e);
}
