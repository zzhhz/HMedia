package com.zzh.lib.recorder.def;

import com.zzh.lib.recorder.HMediaRecorder;

/**
 * Created by ZZH on 2023/1/28.
 *
 * @Date: 2023/1/28
 * @Email: zzh_hz@126.com
 * @QQ: 1299234582
 * @Author: zzh
 * @Description: 播放器状态发生变化回调
 */
public interface OnStateChangeCallback {
    /**
     * 播放器状态发生变化回调
     *
     * @param recorder
     * @param oldState
     * @param newState
     */
    void onStateChanged(HMediaRecorder recorder, HMediaRecorder.State oldState, HMediaRecorder.State newState);
}
