package com.zzh.lib.recorder.utils;


import android.util.Log;

/**
 * Created by ZZH on 2023/1/28.
 *
 * @Date: 2023/1/28
 * @Email: zzh_hz@126.com
 * @QQ: 1299234582
 * @Author: zzh
 * @Description:
 */
public final class HLog {
    private static boolean isMediaRecorder = true;
    private static boolean isMp3Recorder = true;

    private HLog() {
    }

    public static void setIsMediaRecorder(boolean isMediaRecorder) {
        HLog.isMediaRecorder = isMediaRecorder;
    }

    public static void setIsMp3Recorder(boolean isMp3Recorder) {
        HLog.isMp3Recorder = isMp3Recorder;
    }

    public static void isMRLog(String text) {
        if (isMediaRecorder) {
            Log.d("MediaRecorder", text);
        }
    }

    public static void isMp3Log(String text) {
        if (isMp3Recorder) {
            Log.d("Mp3Recorder", text);
        }
    }

    public static void isMRLogE(String text) {
        if (isMediaRecorder) {
            Log.d("MediaRecorder", text);
        }
    }

    public static void isMp3LogE(String text) {
        if (isMp3Recorder) {
            Log.d("Mp3Recorder", text);
        }
    }
}
