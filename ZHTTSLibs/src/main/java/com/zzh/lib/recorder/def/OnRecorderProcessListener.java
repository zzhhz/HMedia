package com.zzh.lib.recorder.def;


import com.zzh.lib.recorder.pcm.PCMRecorder;

/**
 * Created by zzh on 2024/1/5.
 *
 * @date: 2024/1/5 16:13
 * @email: zzh_hz@126.com
 * @author: zzh
 * @description: com.zzh.lib.recorder.def
 */
public interface OnRecorderProcessListener {

    void onProcess(PCMRecorder recorder, byte[] buffer, int len);

    void volumeChange(double volume);

    void onStart(PCMRecorder recorder);

    void onEnd(long duration);
}
