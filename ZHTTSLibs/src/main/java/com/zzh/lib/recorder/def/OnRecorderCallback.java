package com.zzh.lib.recorder.def;


import java.io.File;

/**
 * Created by ZZH on 2023/1/28.
 *
 * @Date: 2023/1/28
 * @Email: zzh_hz@126.com
 * @QQ: 1299234582
 * @Author: zzh
 * @Description: 录音成功回调 HMediaRecorder
 */
public interface OnRecorderCallback {
    /**
     * 录音成功回调
     *
     * @param file
     * @param duration
     */
    void onRecordSuccess(File file, long duration);
}
