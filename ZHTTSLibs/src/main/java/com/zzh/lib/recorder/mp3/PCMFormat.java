package com.zzh.lib.recorder.mp3;

import android.media.AudioFormat;
import android.os.Build;

import androidx.annotation.RequiresApi;

/**
 * 录制格式
 */

public enum PCMFormat {
    PCM_8BIT(1, AudioFormat.ENCODING_PCM_8BIT),
    PCM_16BIT(2, AudioFormat.ENCODING_PCM_16BIT),
    @RequiresApi(api = Build.VERSION_CODES.S)
    PCM_32BIT(3, AudioFormat.ENCODING_PCM_32BIT);

    private final int bytesPerFrame;
    private final int audioFormat;

    PCMFormat(int bytesPerFrame, int audioFormat) {
        this.bytesPerFrame = bytesPerFrame;
        this.audioFormat = audioFormat;
    }

    public int getBytesPerFrame() {
        return bytesPerFrame;
    }

    public int getAudioFormat() {
        return audioFormat;
    }
}
