package com.zzh.lib.htts.callback;


/**
 * Created by zzh on 2023/4/26.
 *
 * @date: 2023/4/26 15:43
 * @email: zzh_hz@126.com
 * @author: zzh
 * @description: com.zzh.lib.htts.callback 设置参数是否成功回调
 */
public abstract class OnSetParamsCallback {

    /**
     * 设置pitch 参数回调结果
     *
     * @param pitch 是否成功, TextToSpeech.SUCCESS; TextToSpeech.ERROR
     */
    public void onSetPitch(int pitch) {

    }

    /**
     * 设置pitch 参数回调结果
     *
     * @param speechRate 是否成功, TextToSpeech.SUCCESS; TextToSpeech.ERROR
     */
    public void onSetSpeechRate(int speechRate) {

    }

    /**
     * 设置 audioAttributes 参数回调结果
     *
     * @param audioAttributes 是否成功, TextToSpeech.SUCCESS; TextToSpeech.ERROR
     */
    public void onSetAudioAttributes(int audioAttributes) {

    }

    /**
     * 设置voice 参数回调结果
     *
     * @param voice 是否成功, TextToSpeech.SUCCESS; TextToSpeech.ERROR
     */
    public void onSetVoice(int voice) {

    }

    /**
     * 设置language 参数回调结果
     *
     * @param language 是否成功, TextToSpeech.SUCCESS; TextToSpeech.ERROR
     */
    public void onSetLanguage(int language) {

    }
}
