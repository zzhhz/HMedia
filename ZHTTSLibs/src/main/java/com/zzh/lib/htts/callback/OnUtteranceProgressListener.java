package com.zzh.lib.htts.callback;


import android.speech.tts.UtteranceProgressListener;

/**
 * Created by zzh on 2024/4/9.
 *
 * @date: 2024/4/9 21:25
 * @email: zzh_hz@126.com
 * @author: zzh
 * @description: com.zzh.lib.htts.callback
 * 文字转语音进度关键点监听。
 */
public class OnUtteranceProgressListener extends UtteranceProgressListener {
    @Override
    public void onStart(String utteranceId) {

    }

    @Override
    public void onDone(String utteranceId) {

    }

    @Override
    public void onError(String utteranceId) {

    }
}
