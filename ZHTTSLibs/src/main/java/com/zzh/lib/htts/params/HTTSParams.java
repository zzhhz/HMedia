package com.zzh.lib.htts.params;

import android.media.AudioAttributes;
import android.speech.tts.UtteranceProgressListener;
import android.speech.tts.Voice;

import java.util.Locale;

/**
 * Created by ZZH on 5/10/21.
 *
 * @Date: 5/10/21
 * @Email: zzh_hz@126.com
 * @QQ: 1299234582
 * @Author: zzh
 * @Description: 语音合成参数。
 */
public class HTTSParams {
    /**
     * 设置合成的语言：默认中文
     */
    public Locale loc = null;
    /**
     * Speech pitch. {@code 1.0} is the normal pitch,
     * lower values lower the tone of the synthesized voice,
     * greater values increase it.
     */
    public float pitch = 1.0f;
    /**
     * 播报速度，1.0正常语速，0.5 正常语速的一半，2 正常速度的二倍。
     */
    public float speechRate = 1.0f;

    public Voice defaultVoice;

    public AudioAttributes audioAttributes;

    public HTTSParams() {
    }

    public HTTSParams(float speechRate) {
        this.speechRate = speechRate;
    }

    public HTTSParams(float pitch, float speechRate) {
        this.pitch = pitch;
        this.speechRate = speechRate;
    }

    public HTTSParams(Locale loc, float pitch, float speechRate, Voice defaultVoice, AudioAttributes audioAttributes) {
        this.loc = loc;
        this.pitch = pitch;
        this.speechRate = speechRate;
        this.defaultVoice = defaultVoice;
        this.audioAttributes = audioAttributes;
    }
}
